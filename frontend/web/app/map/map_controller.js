photolocate.controller('MapController', ['$scope', '$http', 'Map', 'CurrentSerie', 'Count', function ($scope, $http, Map, CurrentSerie, Count) {

    // Initialisation des variables
    var map = L.map('map');
    var markerResponse = L.marker();
    var markerBase = L.marker();
    var save = 0;
    var countMarker = 0;

    $scope.currentSerie = CurrentSerie;
    $scope.count = Count;


    $scope.$watch('currentSerie.serie', function (newValue) {
        if (newValue != 0) {
            createMap(newValue);
        }
    });

    $scope.$watch('count.number', function (newValue, oldValue) {
        if (newValue != 0) {
            next(newValue, oldValue);
        }
    });

    // Création de la map
    var createMap = function (newValue) {
        map.remove();
        map = L.map('map', {zoomControl: false}).setView([newValue.lat, newValue.lon], $scope.currentSerie.serie.zoom);

        $scope.lat = newValue.lat;
        $scope.lon = newValue.lon;

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        // Désactivation des différents contrôles de la map
        map.dragging.disable();
        map.touchZoom.disable();
        map.doubleClickZoom.disable();
        map.scrollWheelZoom.disable();
        map.keyboard.disable();
        map.on('click', $scope.onMapClick);
    };

    //recuperation des lat/lng lors du clic
    $scope.onMapClick = function (e) {
        save = 1;
        lat = $scope.photos[0].lat;
        lon = $scope.photos[0].lon;
        $scope.posOnClick = e.latlng;
        document.getElementById("buttonNext").disabled = false;
        $scope.getDistance();
        $scope.stop();
    };

    //détermine la distance entre le clic et la position de l'image
    $scope.getDistance = function () {
        if (countMarker != 1) {
            var distance = $scope.posOnClick.distanceTo([$scope.photos[0].lat, $scope.photos[0].lon]);
            Map.distance = distance;
            markerResponse = L.marker([$scope.posOnClick.lat, $scope.posOnClick.lng]).addTo(map);
            markerResponse.bindPopup('Votre reponse', {autoPan: false});

            markerBase = L.marker([$scope.photos[0].lat, $scope.photos[0].lon]).addTo(map);
            markerBase.bindPopup('Point correct', {autoPan: false}).openPopup();

            var pointA = new L.LatLng($scope.posOnClick.lat, $scope.posOnClick.lng);
            var pointB = new L.LatLng($scope.photos[0].lat, $scope.photos[0].lon);
            var pointList = [pointA, pointB];

            $scope.line = L.polyline(pointList, {
                color: 'red',
                weight: 3,
                opacity: 0.5,
                smoothFactor: 1

            }).addTo(map);

            countMarker = 1;
        }

    };


    //permet de supprimer l'ancien marqueur lors du clic sur suivant
    var next = function (newValue, oldValue) {
        if (save == 1) {
            map.removeLayer($scope.line);
            map.removeLayer(markerResponse);
            map.removeLayer(markerBase);
            countMarker = 0;
            save = 0;
        }
    };
}]);