photolocate.controller('GamesController', ['$scope', '$http', 'Game', 'Map', 'Count', 'CurrentSerie', '$timeout', function ($scope, $http, Game, Map, Count, CurrentSerie, $timeout) {
    config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    // Fonction permettant de créer un tableau d'images aléatoires et différentes
    function randomInt(mini, maxi) {
        var nb = mini + (maxi + 1 - mini) * Math.random();
        return Math.floor(nb);
    }

    // Fonction permettant de faire un random
    Array.prototype.shuffle = function (n) {
        if (!n)
            n = this.length;
        if (n > 1) {
            var i = randomInt(0, n - 1);
            var tmp = this[i];
            this[i] = this[n - 1];
            this[n - 1] = tmp;
            this.shuffle(n - 1);
        }
    };
    //===============================================================================================================

    //Initialisation des variables
    scoreOrder = [];
    player = [];
    $scope.photos = [];
    $scope.historique = [];
    $scope.map = Map;
    $scope.counter = 10;
    $scope.photoRestant = 1;
    $scope.photoTotal = 0;
    timeScore = 0;
    var stopped;
    tab = [];

    //get sur les photos pour initialiser les variables dans serie.js
    var GetPhotos = function (id) {
        $http.get('../../photolocate/api/series/' + id + '/photos')
            .then(function (response) {
                    response.data.Photos.forEach(function (data) {
                        var newGame = new Game(data);
                        tab.push(newGame);
                    });
                    tab.shuffle(tab.length);     // on mélange les 10 premiers éléments
                    if (CurrentSerie.nbphotos > tab.length) {
                        Materialize.toast('Nombre de photos choisies trop élevé, nombre de photos MAX : ' + tab.length, 5000);
                        CurrentSerie.nbphotos = tab.length;
                    }
                    for (j = 0; j < CurrentSerie.nbphotos; j++) {
                        $scope.photos[j] = tab[j];
                    }
                    $scope.photoTotal = $scope.photos.length;
                },
                function (error) {
                    console.log(error);
                });
    };

    //fonction permettant de mettre à jour la partie dans la BDD (lancée/ en cours/ terminée)
    var game = function (token, path, data) {
        $http.put('../..' + path + '?token=' + token, data, config)
            .success(function (data, status, headers) {
            });
    };

    //fonction appelée lors du clic sur démarrer une partie. Crée la partie sur le serveur
    // et récupère un token.
    // Gère aussi l'affichage du bloc image/map et le compteur
    $scope.showGame = function (pseudo, serie) {
        if (serie != null && pseudo != null) {
            serieId = JSON.parse(serie).id;
            $(document).ready(function () {
                document.getElementById('maps').style.display = "block";
                document.getElementById('series').style.display = "none";
                $http.post('../../photolocate/api/games', {
                        "pseudo": pseudo,
                        "serie": serieId,
                        "nb_photos": CurrentSerie.nbphotos
                    }, config)
                    .success(function (data, status, headers) {
                        GetPhotos(serieId);
                        $scope.score = 0;
                        token = headers('Token');
                        path = headers('Content-Location');
                        game(token, path, {"status": 2});
                        $scope.countdown();
                    }).error(function () {
                });

            });
        }
        else
            Materialize.toast('Entrez un pseudo et choisissez une série', 5000);

    };

    //fonction qui permet d'ajouter les photos utilisées
    // lors de la partie dans la BDD afin d'avoir un historique
    var photoSerie = function (token, path, data) {
        $http.post('../..' + path + '/photos?token=' + token, data, config)
            .success(function (data) {
                console.log(data);
            })
            .error(function (data) {
                console.log(data);
            });
    };

    //watch permettant de mettre à jour le score lors du clic sur la map
    $scope.$watch("map.distance", function (distance) {
        if ($scope.photos[0] == null) {
            i += 1;
        }
        else {
            if (distance < CurrentSerie.serie.distance) {
                $scope.score += 5 * multiple;
                $scope.historique.push([$scope.photos[0], 5 * multiple, 10 - timeScore]);
            }
            else if (distance < (CurrentSerie.serie.distance * 2)) {
                $scope.score += 2 * multiple;
                $scope.historique.push([$scope.photos[0], 2 * multiple, 10 - timeScore]);
            }
            else if (distance < (CurrentSerie.serie.distance * 3)) {
                $scope.score += multiple;
                $scope.historique.push([$scope.photos[0], multiple, 10 - timeScore]);
            }
            else
                $scope.historique.push([$scope.photos[0], 0, 10 - timeScore]);

        }
    });

    // fonction utilisée lors du clic sur 'suivant' afin d'afficher l'image suivante
    // et de regénerer le timer.
    // Permet a la fin de la partie d'appeller la fonction game pour mettre à jour la game dans la BDD
    $scope.next = function () {
        Count.number = Count.number + 1;
        $scope.photoRestant++;
        if ($scope.s == 0)
            $scope.counter = 10;
        $scope.s = 1;
        if (i < $scope.photos.length) {
            $scope.photos[0] = $scope.photos[i++];
        }
        else {
            game(token, path, {"status": 3, "score": $scope.score});
            var test = JSON.stringify($scope.historique);
            photoSerie(token, path, test);
            document.getElementById('maps').style.display = "none";
            document.getElementById('endGame').style.display = "block";

        }
        document.getElementById("buttonNext").disabled = true;
        $scope.countdown();
    };


    //lancement du décompte
    $scope.countdown = function () {
        stopped = $timeout(function () {
            $scope.counter--;
            if ($scope.counter > 0) {
                $scope.countdown();
            }
            else if ($scope.counter == 0) {
                $scope.stop();
            }

        }, 1000);
    };

    //stop le décompte
    $scope.stop = function () {
        timeScore = $scope.counter;
        $timeout.cancel(stopped);

        if ($scope.counter < 5 && $scope.counter > 0) {
            multiple = 1;
        }

        else if ($scope.counter <= 7 && $scope.counter >= 5) {
            multiple = 2;
        }

        else if ($scope.counter <= 10 && $scope.counter >= 8) {
            multiple = 4;
        }

        else {
            multiple = 0;
        }
        $scope.s = 0;
    };
}]);