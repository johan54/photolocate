photolocate.service('Game', [function() {

    //initialisation des variables concernant les photos
    var Game = function (data) {
        this.id = data.photo.id;
        this.serieid = data.photo.serieid;
        this.desc = data.photo.desc;
        this.lat = data.photo.lat;
        this.lon = data.photo.lon;
        this.url = data.photo.url.href;
    };
    
    return Game;
}]);