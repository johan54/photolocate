photolocate.service('Serie', [function() {

    //initialisation des variables
    var Serie = function (data) {
        //var name = [];
        this.name = data.serie.ville;
        this.id = data.serie.id;
        this.distance = data.serie.dist;
        this.lat = data.serie.lat;
        this.lon = data.serie.lon;
        this.zoom = data.serie.zoom;
    };
    return Serie;
}]);