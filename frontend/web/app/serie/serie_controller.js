photolocate.controller('SeriesController', ['$scope', '$http', 'Serie', 'CurrentSerie', function ($scope, $http, Serie, CurrentSerie) {

$scope.chooseDiffilculty='2';

    //get sur les series pour initialiser les variables dans serie.js
    var GetSerie = function () {
        $http.get('../../photolocate/api/series')
            .then(function (response) {
                    $(document).ready(function () {
                        document.getElementById('series').style.display = "inline-block";
                        document.getElementById('maps').style.display = "none";
                    });

                    $scope.series = [];
                    response.data.Series.forEach(function (data) {
                        var newSerie = new Serie(data);
                        $scope.series.push(newSerie);
                    });
                },
                function (error) {
                    console.log(error);
                });
    };
    GetSerie();

    //fonction appelée lors du clic au démarrage de la partie et qui définie les options du jeu
    //comme le nombre de photos ainsi que la distance à avoir entre l'image et le clic
    $scope.showSerie = function (pseudo, serieName, chooseDiffilculty) {
        if (serieName != null && pseudo != null) {
            $(document).ready(function () {
                document.getElementById('maps').style.display = "block";
                document.getElementById('series').style.display = "none";
                CurrentSerie.serie = JSON.parse(serieName);
            });
            switch (chooseDiffilculty) {
                case '1':
                    CurrentSerie.serie.distance = CurrentSerie.serie.distance * 1;
                    CurrentSerie.nbphotos = 5;
                    break;
                case '2':
                    CurrentSerie.serie.distance = CurrentSerie.serie.distance * 0.75;
                    CurrentSerie.nbphotos = 10;
                    break;
                case '3':
                    CurrentSerie.serie.distance = CurrentSerie.serie.distance * 0.5;
                    CurrentSerie.nbphotos = 15;
                    break;
            }

        }
    }
}]);