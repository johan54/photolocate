<?php

require_once '../vendor/autoload.php';

use photolocate\frontend\controller as Controller;

$container = new Slim\Container();

$container['twig'] = function ($container) {
    $loader = new Twig_Loader_Filesystem('web/view');
    return new Twig_Environment($loader, array('debug' => true));
};

photolocate\app\App::DbConf('../src/photolocate/utils/config.ini');

$app = new Slim\App($container);

$app->get('/play', function() use ($app){
    $controller = new Controller\GameController($app);
    return $controller->startGame();
});

$app->run();
