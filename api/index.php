 <?php

require_once '../vendor/autoload.php';
use photolocate\api\controller as Controller;

photolocate\app\App::DbConf('../src/photolocate/utils/config.ini');

$app = new \Slim\App();


 $c = $app->getContainer();
 $c['errorHandler'] = function ($c) {
     return function ($request, $response, $exception) use ($c) {
         $data = [
             'code' => $exception->getCode(),
             'message' => $exception->getMessage(),
             'file' => $exception->getFile(),
             'line' => $exception->getLine(),
             'trace' => explode("\n", $exception->getTraceAsString()),
         ];

         return $c->get('response')->withStatus(500)
             ->withHeader('Content-Type', 'application/json')
             ->write(json_encode($data));
     };
 };
 
$app->group('/photos', function () use ($app) {

    ///////////// Retourne la liste des photos /////////////
    $app->get('', function ($req, $res) use ($app) {
        $controller = new \photolocate\api\controller\PhotoController($req, $res, $app);
        return $controller->getAllPhotos();
    })->setName('getAllPhotos');

    ///////////// Retourne la liste des parties /////////////
    $app->get('/{id}', function ($req, $res, $args) use ($app) {
        $id = $args['id'];
        $controller = new \photolocate\api\controller\PhotoController($req, $res, $app);
        return $controller->getPhotoById($id);
    })->setName('getPhotoById');

    $app->post('', function ($req, $res) use ($app) {
        $postVars = json_decode($req->getBody());
        $controller = new Controller\PhotoController($req, $res, $app);
        $controller->addPhoto($postVars);
    });

        $app->get('/{id}/serie', function ($req, $res, $args) use ($app) {
            $id = $args['id'];
            $controller = new \photolocate\api\controller\SerieController($req, $res, $app);
            return $controller->getSerieByPhotos($id);
        })->setName('getSerieByPhoto');
});

$app->group('/series', function () use ($app) {

    ///////////// Retourne la liste des series /////////////
    $app->get('', function ($req, $res) use ($app) {
        $controller = new Controller\SerieController($req, $res, $app);
        return $controller->getAllSeries();
    });

    ///////////// Retourne une série en fonction de son id /////////////
    $app->get('/{id}', function($req, $res, $args) use ($app){
        $id = $args['id'];
        $controller = new Controller\SerieController($req, $res, $app);
        return $controller->getSerieById($id);
    })->setName('serie');

    $app->get('/{id}/photos', function($req, $res, $args) use ($app){
       $id = $args['id'];
        $controller = new Controller\SerieController($req, $res, $app);
        return $controller->getPhotosInSerie($id);
    })->setName('photosInSerie');
});

$app->group('/games', function () use ($app) {

    ///////////// Retourne la liste des parties /////////////
    $app->get('', function ($req, $res) use ($app) {
        $controller = new Controller\PartieController($req, $res, $app);
        return $controller->getAllGames();
    })->setName('getAllGames');

    ///////////// Retourne une partie en fonction de son id  /////////////
    $app->get('/{id}', function($req, $res, $args) use ($app){
        $id = $args['id'];
        $controller = new Controller\PartieController($req, $res, $app);
        return $controller->getGameById($id);
    })->setName('getGameById');

    ///////////// Création d'une partie avec génération de token /////////////
    $app->post('', function($req, $res) use ($app){
        $postVars = json_decode($req->getBody());
        $controller = new Controller\PartieController($req, $res, $app);
        return $controller->createGame($postVars);
    });

    ///////////// Changement du status d'une partie et ajout du score si status = 3 /////////////
    $app->put('/{id}', function($req, $res, $args) use ($app){
        $token = $req->getQueryParams()['token'];
        $id = $args['id'];
        $postVars = json_decode($req->getBody());
        $controller = new Controller\PartieController($req, $res, $app);
        return $controller->updateGame($id, $postVars, $token);
    });

    $app->post('/{id}/photos', function($req, $res, $args)use ($app) {
        $token = $req->getQueryParams()['token'];
        $id = $args['id'];
        $postVars = json_decode($req->getBody());
        $controller = new Controller\PartieController($req, $res, $app);
        return $controller->postPhotosInGame($id, $postVars, $token);
    });

        $app->get('/{id}/photos', function($req, $res, $args)use ($app){
        $id = $args['id'];
        $controller = new Controller\PartieController($req, $res, $app);
            return $controller->getPhotosByGame($id);
    })->setName('idPhotoByGame');
});

$app->run();