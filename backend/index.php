<?php

require_once '../vendor/autoload.php';

$container = new Slim\Container();

$container['twig'] = function ($container) {
    $loader = new Twig_Loader_Filesystem('web/view');
    return new Twig_Environment($loader, array('debug' => true));
};

photolocate\app\App::DbConf('../src/photolocate/utils/config.ini');

$app = new Slim\App($container);

$c = $app->getContainer();

//Slim 3 error handler
$c['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        $data = [
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => explode("\n", $exception->getTraceAsString()),
        ];

        return $c->get('response')->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode($data));
    };
};

use photolocate\backend\controller as Controller;

$app->group('/', function () use ($app){

    // Affichage de index.html
    $app->get('', function () use ($app) {
        $controller = new Controller\SerieController($app);
        return $controller->getAllSeries();
    });

    // Envoie du formulaire d'upload d'image
    $app->post('', function ($req, $res) use ($app) {
        //Récupération du body de la requête
        $postVars = $req->getParsedBody();

        //On regarde si on reçoit le formulaire 'photo'
        if(isset($postVars['photoForm'])){
            $controller = new Controller\PhotoController();
            $upload = $controller->uploadPhoto($postVars);
            return $res->withRedirect('../backend/?state='.$upload);
        }

        //On regarde si on reçoit le formulaire 'série'
        if(isset($postVars['serieForm'])){
            $controller = new Controller\SerieController($app);
            $upload = $controller->insertSerie($postVars);
            return $res->withRedirect('../backend/?state='.$upload);
        };
    });
});

$app->run();
