//Récupération des paramètres dans l'URL
var urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
};

//Définition de l'objet map
var Map = function () {
    this.map = null;
    this.markerLat = null;
    this.markerLon = null;
    this.circle = null;
};

//Création de la carte
Map.prototype.createMap = function () {
    _this = this;
    this.map = L.map('map').setView([46.6812208, 2.3573614], 5);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(_this.map);

    _this.map.on('click', onMapClick);
    _this.map.on("zoomend", onZoomEnd);
};

//Récupération de la latitude et longitude
Map.prototype.getLatLon = function (value) {
    _this = this;
    $.get('../api/series/' + value, function (data) {
        _this.map.setView([data.serie.lat, data.serie.lon], 13);
    });
};

//Fonction permettant de récupérer les coordonnées d'une ville
Map.prototype.search = function () {
    _this = this;
    var searchTxt = $('#addr').val();

    $.getJSON('http://nominatim.openstreetmap.org/search?format=json&limit=1&q=' + searchTxt, function (data) {
        //On stocke les coordonnées pour le futur marker
        _this.markerLat = data[0].lat;
        _this.markerLon = data[0].lon;

        _this.map.setView([data[0].lat, data[0].lon], 13);
        L.marker([_this.markerLat, _this.markerLon]).addTo(_this.map);

        //Effacement du label et remplissage des inputs
        $("label[for=latitude], label[for=longitude]").text("");
        $("input[name=latitude]").val(data[0].lat);
        $("input[name=longitude]").val(data[0].lon);
    });
};

//Ajout d'un cercle pour visualiser la distance entre 2 points
Map.prototype.addCircle = function (value) {
    _this = this;
    if (_this.circle != null) {
        _this.map.removeLayer(_this.circle);
    }
    //On enregistre le cercle pour pouvoir le supprimer a chaque appel
    _this.circle = L.circle([_this.markerLat, _this.markerLon], value, {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.5
    }).addTo(_this.map);
};

//Affichage des messages d'erreur ou succès
var displayMessage = function () {
    var url = urlParam('state');

    if (url == 'success') {
        $('#success').show();
    }
    if (url == 'failed') {
        $('#failed').show();
    }
};

//Modification des labels 'latitude' et 'longitude'
var onMapClick = function (e) {
    //On efface les labels pour ne pas que les valeurs se superposent
    $("label[for=lat], label[for=lon]").text("");
    //Affichage des valeurs
    $("input[name=lat]").val(e.latlng.lat);
    $("input[name=lon]").val(e.latlng.lng);
};

//Récupération du zoom
var onZoomEnd = function (e) {
    $("label[for=zoom]").text("");
    $("input[name=zoom]").val(e.target._zoom);
};

//Quand on clique sur un onglet
$('.tabs').click(function () {
    //On regarde le quel est actif
    var activeTab = $('.tabs').find('.active');
    //Si c'est l'onget Photo
    if (activeTab[0].innerHTML === "Photo") {
        //On cache la barre de recherche
        document.getElementById('rowSearch').style.display = "none";
    }
    else {
        //Sinon on l'affiche
        document.getElementById('rowSearch').style.display = "block";
    }
});

//Lorsqu'on tape du texte dans la recherche il est copié dans la ville
var $ville = $("#ville");
$('#addr').keyup(function () {
    $("label[for=ville]").text("");
    $ville.val(this.value);
});

//Quand on change de valeur avec le select
$('select').change(function () {
    map.getLatLon(this.value);
});

//Quand on clique sur le bouton 'rechercher' de la carte
$('#search').click(function () {
    map.search();
});

$('#dist').keyup(function () {
    map.addCircle($('#dist').val());
});

$(function () {
    //Initilisations pour Materialize
    $('select').material_select();
    $('ul.tabs').tabs();

    //On cache la barre de recherche pour la carte
    document.getElementById('rowSearch').style.display = "none";

    //Après l'upload de photo on affiche si tout s'est bien passé ou non
    displayMessage();

    //Création de la map
    map = new Map();
    map.createMap();
});