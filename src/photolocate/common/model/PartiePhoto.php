<?php

namespace photolocate\common\model;

use Illuminate\Database\Eloquent\Model;

class PartiePhoto extends Model
{
    protected $table = 'partie_photo';
    protected $primaryKey = 'id';
    public $timestamps = false;
}