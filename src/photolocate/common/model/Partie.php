<?php

namespace photolocate\common\model;

use Illuminate\Database\Eloquent\Model;

class Partie extends Model
{
    protected $table = 'partie';
    protected $primaryKey = 'id';
    public $timestamps = false;
}