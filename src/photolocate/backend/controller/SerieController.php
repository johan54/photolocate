<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 01/02/16
 * Time: 15:48
 */

namespace photolocate\backend\controller;

use photolocate\common\model\Serie;

class SerieController
{
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    //Récupération de toutes les séries
    public function getAllSeries()
    {
        $template = $this->app->getContainer()->get('twig')->loadTemplate('index.html');
        $series = Serie::all();
        return $template->render(array('series' => $series));
    }

    //Insertion d'une série
    public function insertSerie($postVars)
    {
        // Try de l'insertion
        try {
            $serie = new Serie();
            $serie->ville = $postVars['city'];
            $serie->dist = $postVars['distance'];
            $serie->lat = $postVars['latitude'];
            $serie->lon = $postVars['longitude'];
            $serie->zoom = $postVars['zoom'];
            $serie->save();

            return 'success';

        } catch (\Exception $e) {
            // Fail!
            return 'failed';
        }
    }
}