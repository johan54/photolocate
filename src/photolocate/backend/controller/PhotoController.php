<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 01/02/16
 * Time: 16:36
 */

namespace photolocate\backend\controller;

use photolocate\common\model\Photo;
use Upload\File;
use Upload\Storage\FileSystem;
use Upload\Validation\Mimetype;

class PhotoController
{
    public function uploadPhoto($postVars)
    {
        //Répertoire ou sera déplacée l'image
        $storage = new FileSystem('../frontend/web/photo');

        $file = new File('picture', $storage);

        // Renommage du fichier
        $new_filename = uniqid();
        $file->setName($new_filename);

        //Type pris en compte pour la validation du fichier
        $file->addValidations(array(
            new Mimetype(array('image/png', 'image/gif', 'image/jpeg'))
        ));

        // Try de l'upload
        try {
            // Success!
            $file->upload();

            $photo = new Photo();
            $photo->url = $file->getNameWithExtension();
            $photo->desc = $postVars['description'];
            $photo->serieid = $postVars['series'];
            $photo->lat = $postVars['lat'];
            $photo->lon = $postVars['lon'];
            $photo->save();

            return 'success';

        } catch (\Exception $e) {
            // Fail!
            return 'failed';
        }
    }
}