<?php

namespace photolocate\api\controller;

use photolocate\common\model\Photo;
use photolocate\common\model\Serie;

class SerieController extends AbstractController
{
    public function getAllSeries(){
        $res = [];
        $router = $this->app->getContainer()->get('router');
        //Récupếration de toutes les séries
        $series = Serie::all();

        foreach($series as $serie){
            $res[] = ['serie' => $serie, 'links' => ['self' => ['href' => $router->pathFor('serie',['id' => $serie->id])]]];
        }

        //Finalisation du format json
        $tab = array('Series' => $res, 'Links' => []);

        $encoded = json_encode($tab);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);

        return $response;
    }

    public function getSerieById($id){

        $router = $this->app->getContainer()->get('router');

        $serie = Serie::find($id);

        if(empty($serie)){
            $res = ['codeErreur' => 404,
                'messageErreur' => "La ressource demandée n'a pas été trouvée",
                'ressourceDemandee' => $router->pathFor('serie', ['id' => $id])];
            $encoded = json_encode($res);

            //Ecriture du header
            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 404);
            $response = $this->Write($response, $encoded);

            return $response;
        }
        $res = ['serie' => $serie, 'links' => ['photos' => ['href' => $router->pathFor('photosInSerie',['id' => $id])]]];
        $encoded = json_encode($res);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);

        return $response;
    }

    public function getPhotosInSerie($id)
    {

        $router = $this->app->getContainer()->get('router');

        $photos = Photo::select('*')->where('serieid', '=', $id)->get();

        if (empty($photos[0])) {

            $res = ['codeErreur' => 404,
                'messageErreur' => "La ressource demandée n'a pas été trouvée",
                'ressourceDemandee' => $router->pathFor('photosInSerie', ['id' => $id])];
            $encoded = json_encode($res);

            //Ecriture du header
            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 404);
            $response = $this->Write($response, $encoded);

            return $response;
        }

        foreach ($photos as $p) {
            $res[] = ['photo' => ['id' => $p->id, 'desc' => $p->desc, 'lat' => $p->lat, 'lon' => $p->lon, 'serieid' => $p->serieid, 'url' =>
                ['href' => '/photolocate/frontend/web/photo/' . $p->url]], 'links' => ['self' => ['href' => $router->pathFor('getPhotoById', ['id' => $p->id])]]];
        }

        $tab = array('Photos' => $res, 'Links' => []);
        $encoded = json_encode($tab);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);
        return $response;
    }

    public function getSerieByPhotos($id)
    {
        $router = $this->app->getContainer()->get('router');
        $serie = Serie::join('photo', 'serie.id', '=', 'photo.serieid')
            ->where('photo.id', '=', $id)
            ->select('serie.id', 'serie.ville', 'serie.dist', 'serie.lat', 'serie.lon')
            ->get();
        foreach ($serie as $s) {
            $res = ['serie' => $serie, 'links' => ['photos' => ['href' => $router->pathFor('photosInSerie', ['id' => $s->id])]]];
            $encoded = json_encode($res);
        }

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);
        return $response;
    }
}
