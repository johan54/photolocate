<?php

namespace photolocate\api\controller;

use photolocate\common\model\Photo;

class PhotoController extends AbstractController
{
    public function getAllPhotos()
    {
        $res = [];
        $router = $this->app->getContainer()->get('router');
        $photo = Photo::all();

        foreach ($photo as $p) {
            $res[] = ['photo' => ['id' => $p->id, 'desc' => $p->desc, 'lat' => $p->lat, 'lon' => $p->lon, 'serieid' => $p->serieid, 'url' =>
                ['href' => '/photolocate/frontend/web/photo/' . $p->url]], 'links' => ['self' => ['href' => $router->pathFor('getPhotoById', ['id' => $p->id])]]];
        }

        $tab = array('Photos' => $res, 'Links' => []);

        $encoded = json_encode($tab);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);
        return $response;
    }

    public function getPhotoById($id)
    {
        $router = $this->app->getContainer()->get('router');
        $photo = Photo::find($id);

        if ($photo) {

            $res = ['photo' => ['id' => $photo->id, 'desc' => $photo->desc, 'lat' => $photo->lat, 'lon' => $photo->lon, 'serieid' => $photo->serieid, 'url' =>
                ['href' => '/photolocate/frontend/web/photo/' . $photo->url]], 'link' => ['serie' => ['href' => $router->pathFor('getSerieByPhoto', ['id' => $photo->id])]]];

            $encoded = json_encode($res);

            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 200);
            $response = $this->Write($response, $encoded);
            return $response;
        } else {
            $res = ['codeErreur' => 404,
                'messageErreur' => "La ressource demandée n'a pas été trouvée",
                'ressourceDemandee' => $router->pathFor('getPhotoById', ['id' => $id])];
            $encoded = json_encode($res);

            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 404);
            $response = $this->Write($response, $encoded);

            return $response;
        }
    }
}