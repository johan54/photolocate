<?php

namespace photolocate\api\controller;


use photolocate\common\model\Partie;
use photolocate\common\model\PartiePhoto;
use photolocate\common\model\Photo;
use RandomLib\Factory;

class PartieController extends AbstractController
{
    public function getAllGames()
    {
        $res = [];
        $router = $this->app->getContainer()->get('router');
        $game = Partie::all();

        foreach ($game as $g) {
            $res[] = ['game' => ['id' => $g->id, 'nb_photos' => $g->nb_photos, 'status' => $g->status, 'score' => $g->score, 'joueur' => $g->joueur, 'serieid' => $g->serieid],'links' => ['self' => ['href' => $router->pathFor('getGameById', ['id' => $g->id])]]];
        }

        $tab = array('Games' => $res, 'Links' => []);

        $encoded = json_encode($tab);

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->Status($response, 200);
        $response = $this->Write($response, $encoded);
        return $response;
    }

    public function getGameById($id)
    {
        $router = $this->app->getContainer()->get('router');
        $game = Partie::find($id);
            if($game) {
                $res = ['game' => $game, 'link' => ['serie' => ['href' => $router->pathFor('serie', ['id' => $game->serieid])], 'photo' => ['href' => $router->pathFor('idPhotoByGame', ['id' => $id])]]];

                $encoded = json_encode($res);

                $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
                $response = $this->Status($response, 200);
                $response = $this->Write($response, $encoded);
                return $response;
            }
            else{
                $res = ['codeErreur' => 404,
                    'messageErreur' => "La ressource demandée n'a pas été trouvée",
                    'ressourceDemandee' => $router->pathFor('getGameById', ['id' => $id])];
                $encoded = json_encode($res);

                $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
                $response = $this->Status($response, 404);
                $response = $this->Write($response, $encoded);

                return $response;
            }
        }

    public function createGame($postVars)
    {
        $router = $this->app->getContainer()->get('router');

        $factory = new Factory();
        $generator = $factory->getMediumStrengthGenerator();

        // Création du token
        $token = $generator->generateString(64, 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN1234567890');

        $game = new Partie();

        $game->token = $token;
        $game->nb_photos = $postVars->nb_photos;
        $game->status = 1;
        $game->joueur = $postVars->pseudo;
        $game->serieid = $postVars->serie;

        //Création de la partie
        $game->save();

        $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
        $response = $this->jsonHeader($response, 'Content-Location', $router->pathFor('getGameById', ['id' => $game->id]));
        $response = $this->jsonHeader($response, 'Token', $token);
        $response = $this->Status($response, 201);
        $response = $this->Write($response, '');
        return $response;
    }

    public function updateGame($id, $postVars, $token)
    {
        $game = Partie::find($id);

        if ($token == $game->token) {
            $game->status = $postVars->status;

            if($postVars->status == 3){
                $game ->score = $postVars->score;
            }

            $game->save();
        }

        else{

            $tab = array('codeErreur' => 401,
                        'messageErreur' => 'Veuillez entrer un token valide.');

            $encoded = json_encode($tab);

            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 401);
            $response = $this->Write($response, $encoded);
            return $response;
        }
    }

    public function postPhotosInGame($id, $postVars, $token)
    {
        foreach ($postVars as $p) {
            $photosgame = new PartiePhoto();

            $photosgame->partieid = $id;
            $photosgame->photoid = $p[0]->id;

            $photosgame->save();
        }
    }

    public function getPhotosByGame($id) {
        $res = [];
        $router = $this->app->getContainer()->get('router');
        $photo = Photo::join('partie_photo', 'photo.id', '=', 'partie_photo.photoid')->select('*')->where('partie_photo.partieid', '=', $id)->get();


        if(!empty($photo[0])) {
            foreach ($photo as $p) {
                $res[] = ['photo' => ['id' => $p->photoid, 'desc' => $p->desc, 'lat' => $p->lat, 'lon' => $p->lon, 'serieid' => $p->serieid, 'url' =>
                    ['href' => '/photolocate/frontend/web/photo/' . $p->url]], 'links' => ['self' => ['href' => $router->pathFor('getPhotoById', ['id' => $p->photoid])]]];
            }

            $tab = array('Photos' => $res, 'Links' => []);

            $encoded = json_encode($tab);
            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 401);
            $response = $this->Write($response, $encoded);
            return $response;
        }
        else{
            $res = ['codeErreur' => 404,
                'messageErreur' => "La ressource demandée n'a pas été trouvée",
                'ressourceDemandee' => $router->pathFor('idPhotoByGame', ['id' => $id])];
            $encoded = json_encode($res);

            $response = $this->jsonHeader($this->response, 'Content-Type', 'application/json');
            $response = $this->Status($response, 404);
            $response = $this->Write($response, $encoded);

            return $response;
        }
    }
}