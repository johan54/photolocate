<?php

namespace photolocate\frontend\controller;

class GameController
{
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function startGame(){
        $template = $this->app->getContainer()->get('twig')->loadTemplate('index.html');
        return $template->render(array());
    }
}